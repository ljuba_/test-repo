<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress_colotheme');

/** MySQL database username */
define('DB_USER', 'user1');

/** MySQL database password */
define('DB_PASSWORD', '12345678');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '-|69WO>Fbttw&V|*86ZP`{NTL3ZKMA%u(]O~;|$2KNyWR!E966-[Id.9K])w<~y!');
define('SECURE_AUTH_KEY',  'r-kZEJYTxk*6q$(<|{*32f/-ca^z9_`#RVZpIfnw}p|EZw-5Fb;?`TL5n2y/ZeY4');
define('LOGGED_IN_KEY',    '$Kh~#6o^8m}l`BnJR/&i&la*n`x!El!m+|+r.y5rbl[Cir`T3dgcK%?8+VnPZkIe');
define('NONCE_KEY',        'kX#5;#J?,H[X6k;5wuF0w59Jp%lJ|8gl|/!)S?&lDDQ<{5Ar*ML9EBjYly0=5SN&');
define('AUTH_SALT',        '3mAO-Q.3<:+Ck6>k[fn?&WH_j*^o=%.LyJd1kPJ3)-P|;5tz8h|.Te^kk{{w-`sc');
define('SECURE_AUTH_SALT', 'NLrHYy%JG2c]ys=h+k>K@FC;A9I!RRV:L5;w.~QS[A_35/oC[B:NloLjhTFYyzdA');
define('LOGGED_IN_SALT',   'z`hBu79k[wh~JB=R;q:QcFT%|w2sp[WqP6g{YI#!;~^+;[IU:J0U/qm+>*T-@S@R');
define('NONCE_SALT',       'a[@|^Y*+Iv-4l_N6?S43L]LfJ9t]S|;:`Kg{QWSdg~n_2?{x6Yf?QIvVL48mvH}-');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
