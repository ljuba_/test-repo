<?php
/**
 * Plugin Name: Ajax Test
 * Plugin URI: http://danielpataki.com
 * Description: This is a plugin that allows us to test Ajax functionality in WordPress
 * Version: 1.0.0
 * Author: Daniel Pataki
 * Author URI: http://danielpataki.com
 * License: GPL2
 */

if(preg_match('#' . basename(__FILE__) . '#', $_SERVER['PHP_SELF'])) { die('You are not allowed to call this page directly.'); }


if(!class_exists('BSPInspect')) {
    class BSPInspect {

        // Хранение внутренних данных
        public $data = array();

        function BSPInspect() {
            //Название файла нашего плагина
            $this->plugin_name = plugin_basename(__FILE__);
            $this->plugin_url = trailingslashit(WP_PLUGIN_URL.'/'.dirname(plugin_basename(__FILE__)));

            ## Объявляем константу инициализации нашего плагина
            DEFINE('BSPInspect', true);

            //Функция которая исполняется при активации плагина
            register_activation_hook($this->plugin_name, 'activate');

            // Функция которая исполняется при деактивации плагина
            register_deactivation_hook($this->plugin_name, 'deactivate');

            //  Функция которая исполняется удалении плагина
            register_uninstall_hook($this->plugin_name, 'uninstall');

            // Если мы в адм. интерфейсе
            if ( is_admin() ) {

                // Добавляем стили и скрипты
                add_action('wp_footer', 'admin_load_scripts');
                add_action('wp_header',  'admin_load_styles');

                // Добавляем меню для плагина
                //( 'admin_menu', array(&$this, 'admin_generate_menu') );

            } else {
                // Добавляем стили и скрипты
                add_action('wp_print_scripts', array(&$this, 'site_load_scripts'));
                add_action('wp_print_styles', array(&$this, 'site_load_styles'));

                //add_shortcode('show_reviews', array (&$this, 'site_show_reviews'));
            }

            //ACTIONS

            add_action( 'bsp-inspect', 'bsp_inspect_search' ); // Write our JS below here

            // shortcode

            //Создадим shortcode для вывода HTML-текста на сайте
            add_shortcode("bsp-search", array (&$this, 'searchform_init_shortcode'));
        }

        /**
         * Загрузка необходимых скриптов для страницы управления
         * в панели администрирования
         */
        function admin_load_scripts() {
            wp_register_script('jquery', $this->plugin_url . 'js/jquery-js/jquery-2.1.4.min.js' );
            wp_register_script('search_btn', $this->plugin_url . 'js/search_btn.js' );
            //wp_register_script('advReviewsAdminJs', $this->plugin_url . 'js/admin-scripts.js' );

            wp_enqueue_script('jquery');
            wp_enqueue_script('search_btn');
        }

        /**
         * Загрузка необходимых sryle для страницы управления
         * в панели администрирования
         */
        function admin_load_styles() {
            wp_register_style('search_style', $this->plugin_url . 'css/search_style.css' );
            wp_enqueue_style('search_style');
        }

        function site_load_scripts(){
            wp_register_script('jquery', $this->plugin_url . 'js/jquery-2.1.4.min.js' );
            wp_register_script('search_btn_js', $this->plugin_url . 'js/search_btn.js' );
            wp_enqueue_script('jquery');
            wp_enqueue_script('search_btn_js');
        }

        function site_load_styles() {
            wp_register_style('search_style_btn', $this->plugin_url . 'css/search_style.css' );
            wp_enqueue_style('search_style_btn');
        }

        /*
         * При вводе в #bsp-input выводит сообщение в консоль
         * */
        function bsp_inspect_search($args){
            ?>
            <script>
                $(document).ready(function(){
                    $("#bsp-input").on('input', function () {
                        <?php
                            echo "console.log('message1');";
                        ?>

                    });});
                console.log('ok1');
            </script>
        <?php
        }

        // Вывод лупы на страничке
        function searchform_init_shortcode($atts){
            ?>
            <img class="bsp-inspect-btn" src="<?php
            echo $this->plugin_url; ?>img/search.png" />
            <div class="hidden-search-form">
                <img class="bsp-inspect-close-btn" src="<?php
                echo $this->plugin_url; ?>img/close_btn.png" />
                DIV to hide
            </div>

        <?php
        }

        /**
         * Активация плагина
         */
        function activate() {
            global $wpdb;
        }

        function deactivate() {
            return true;
        }

        /**
         * Удаление плагина
         */
        function uninstall() {
            global $wpdb;
           // $wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}adv_reviews");
        }


        // Получение постов с совпадением в введенном тексте
        function consist($substr, $atts){
            global $wpdb;
            $table_posts = $wpdb->prefix . "posts";

            $results = $wpdb->get_results(
                $wpdb->prepare(
                    "SELECT ID, post_date, post_title FROM $table_posts WHERE %s",
                    "%" . $substr . "%"
                )
            );
            return $results;
        }

    }
}

global $inspect;
$inspect = new BSPInspect();