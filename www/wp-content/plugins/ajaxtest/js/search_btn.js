(function($){})(jQuery);
//search-btn HERE
$(document).ready(function(){

    function resetSearchResult() {
        $('.search-container').html('');
        $('.search__no_result').css('display', 'none');
        $('.search__content .search-preloader').css('display', 'none');
    }

    $('input.search__input').keyup(function(){
        resetSearchResult();
        chunk_word = this.value;

        if (chunk_word.length < 3) {
            return;
        }
        $('.search__content .search-preloader').css('display', 'block');
        $.ajax({
            url: url.ajax,
            type: 'POST',
            data: {action: 'ajax_search', chunk_word : chunk_word},
            success: function(data) {

                try {
                    posts = $.parseJSON(data);
                    if (posts.length < 1) {
                        $('.search__content .search-preloader').css('display', 'none');
                        $('.search__no_result').css('display', 'block');

                        return;
                    }
                    mainHtml = '';
                    $.each(posts, function(index, post) {
                        mainHtml += nano(search_result_tpl, post);
                    });
                    $('.search-container').html(mainHtml);
                    $('.search__content .search-preloader').css('display', 'none');
                } catch (e) {
                    $('.search__content .search-preloader').css('display', 'none');
                }
            },
            fail: function() {
                $('.search__content .search-preloader').css('display', 'none');
            }

        });

    });

    $('img.bsp-inspect-btn').click(function(){
        console.log('click bsp-inspect-btn');
        $(".hidden-search-form").css("display", "block");
        resetSearchResult();
    });

    $('.bsp-inspect-close-btn').click(function(){
        console.log('click bsp-inspect-close-btn');
        $(".hidden-search-form").css("display", "none");
        resetSearchResult();
    });
});