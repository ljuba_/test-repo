<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage COLO Theme
 * @since Twenty Fifteen 1.0
 */
echo 404;