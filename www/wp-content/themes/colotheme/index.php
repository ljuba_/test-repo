<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title><?php wp_title(''); ?> | <?php bloginfo('name'); ?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="shortcut icon" href="favicon.png" />
    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
<!--    <link rel="stylesheet" href="--><?php //echo get_template_directory_uri(); ?><!--/libs/bootstrap/bootstrap-grid.min.css" />-->
<!--    <link rel="stylesheet" href="--><?php //echo get_template_directory_uri(); ?><!--/libs/font-awesome/css/font-awesome.min.css" />-->
<!--    <link rel="stylesheet" href="--><?php //echo get_template_directory_uri(); ?><!--/libs/linea/styles.css" />-->
<!--    <link rel="stylesheet" href="--><?php //echo get_template_directory_uri(); ?><!--/libs/magnific-popup/magnific-popup.css" />-->
<!--    <link rel="stylesheet" href="--><?php //echo get_template_directory_uri(); ?><!--/libs/animate/animate.min.css" />-->
<!--    <link rel="stylesheet" href="--><?php //echo get_template_directory_uri(); ?><!--/css/fonts.css" />-->
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/main.css" />
<!--    <link rel="stylesheet" href="--><?php ////echo get_template_directory_uri(); ?><!--<!--/css/skins/--><?php
//    $options = get_option('sample_theme_options');
//    echo $options['selectinput'];
//    ?><!--.css" />-->
<!--    <link rel="stylesheet" href="--><?php //echo get_template_directory_uri(); ?><!--/css/media.css" />-->

    <?php
    wp_head();
    ?>
</head>

<body>
<?php
do_action("bsp-inspect");
?>
<header>
    Main Header
</header>
<!--<form action="search.php" method="post" name="form" onsubmit="return false;">-->
<!--    <p>-->
<!--        Живой поиск:<br>-->
<!--        <input name="search" type="text" id="search">-->
<!--        <small>Вводите на английском языке</small>-->
<!--    </p>-->
<!--</form>-->
<!--<div id="resSearch">Начните вводить запрос</div>-->
<article>
    <?php if(have_posts()): ?>
    <?php while(have_posts()): the_post();?>
        <section>
            <h1><?php the_title();?></h1>
            <?php the_content();?>
        </section>
    <?php endwhile; endif; ?>

</article>
<footer>
    my footer
</footer>
</body>